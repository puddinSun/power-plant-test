package com.power.plant.battery;

import com.power.plant.domain.battery.repo.BatteryRepository;
import com.power.plant.domain.battery.repo.entity.BatteryEntity;
import com.power.plant.domain.battery.repo.entity.BatteryPageQueryEntity;
import com.power.plant.domain.battery.repo.entity.SearchResultEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.core.publisher.Flux;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@DataMongoTest(properties = {"spring.mongodb.embedded.version=4.0.2"})
@ExtendWith(SpringExtension.class)
public class BatteryRepositoryTest {

   @Autowired
   private BatteryRepository repository;

   @Test
   @DisplayName("AGGREGATION >> Search >> ReturnCustomObject")
   void test() {
      Flux<BatteryEntity> batteryEntityFlux = repository.saveAll(intiData());
      Assertions.assertEquals(batteryEntityFlux.count().block(), 5);

      SearchResultEntity search = repository.search(
            BatteryPageQueryEntity.builder()
                  .pageNumber(0)
                  .pageSize(10)
                  .pCodeRange(Arrays.asList("10001", "10004"))
                  .build())
            .block();

      Assertions.assertNotNull(search);
      Assertions.assertEquals(search.getTotal(), 4);
   }

   private List<BatteryEntity> intiData() {

      List<BatteryEntity> list = new ArrayList<>();
      for (int i = 1; i <= 5; i++) {
         list.add(BatteryEntity.builder()
               .name("bat-" + i)
               .postcode("1000" + i)
               .createdDate(Instant.now())
               .wattCapacity(BigDecimal.valueOf(2000))
               .build());
      }
      return list;
   }

}
