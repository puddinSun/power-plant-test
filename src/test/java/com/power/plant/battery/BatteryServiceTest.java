package com.power.plant.battery;

import com.power.plant.common.throwable.BaseResponseEnum;
import com.power.plant.common.throwable.BizException;
import com.power.plant.domain.battery.mapper.BatteryMapper;
import com.power.plant.domain.battery.pojo.BatteryDTO;
import com.power.plant.domain.battery.pojo.BatteryPageResponse;
import com.power.plant.domain.battery.pojo.BatterySearchQuery;
import com.power.plant.domain.battery.repo.BatteryRepository;
import com.power.plant.domain.battery.repo.entity.BatteryEntity;
import com.power.plant.domain.battery.repo.entity.SearchResultEntity;
import com.power.plant.domain.battery.service.impl.BatteryServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyIterable;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class BatteryServiceTest {

   @Mock
   private BatteryRepository repository;

   @InjectMocks
   private BatteryServiceImpl batteryService;

   private final BatteryMapper mapper = new BatteryMapper();

   @Test
   @DisplayName("POST >> /api/v1/batteries >> ThenReturnList")
   public void bulkCreateBatteries_ThenReturnList() {

      List<BatteryDTO> batteries = intiMockBatteries();
      List<BatteryEntity> entities = batteries.stream()
            .map(mapper::toEntity)
            .collect(Collectors.toList());

      when(repository.saveAll(anyIterable())).thenReturn(Flux.fromIterable(entities));

      Flux<BatteryDTO> flux = batteryService.insertBatch(batteries);
      Assertions.assertNotNull(flux);
      Assertions.assertEquals(flux.count().block(), batteries.size());
   }

   @Test
   @DisplayName("POST >> /api/v1/batteries >> EmptyList >> ThenReturnEmpty")
   public void bulkCreateBatteries_EmptyList_ThenReturnEmpty() {

      List<BatteryDTO> batteries = Collections.emptyList();
      when(repository.saveAll(anyIterable())).thenReturn(Flux.empty());

      Flux<BatteryDTO> flux = batteryService.insertBatch(batteries);
      Assertions.assertEquals(flux.count().block(), 0);
   }

   @Test
   @DisplayName("POST >> /api/v1/batteries/query >> Success")
   public void queryBatteries_Success() {

      when(repository.search(any())).thenReturn(mockResultSet());
      Mono<BatteryPageResponse> query = batteryService.query(initSearchQuery());

      Assertions.assertEquals(Objects.requireNonNull(query.block()).getTotal(), 5L);
      Assertions.assertEquals(Objects.requireNonNull(query.block()).getAvgUsage(), BigDecimal.valueOf(7.5));
      Assertions.assertEquals(Objects.requireNonNull(query.block()).getData().size(), 5);
   }

   @Test
   @DisplayName("POST >> /api/v1/batteries/query >> WithoutPCodeRange >> Success")
   public void queryBatteries_WithoutPCodeRange_Success() {
      BatterySearchQuery searchQuery = initInvalidSearchQuery();

      BizException exception = Assertions.assertThrows(BizException.class, () -> {
         batteryService.query(searchQuery);
      });

      Assertions.assertNotNull(exception.getErrorEnum());
      Assertions.assertEquals(exception.getErrorEnum().getCode(), BaseResponseEnum.PARAMS_ARE_MISSING.getCode());
      Assertions.assertEquals(exception.getErrorEnum().getMsg(), BaseResponseEnum.PARAMS_ARE_MISSING.getMsg());
   }

   private List<BatteryDTO> intiMockBatteries() {

      List<BatteryDTO> list = new ArrayList<>();
      for (int i = 1; i <= 5; i++) {
         list.add(BatteryDTO.builder()
               .name("bat-" + i)
               .postcode("1000" + i)
               .wattCapacity(BigDecimal.valueOf(2000))
               .build());
      }
      return list;
   }

   private BatterySearchQuery initSearchQuery() {
      return BatterySearchQuery.builder()
            .pageSize(5)
            .pageNumber(0)
            .sortBy("name")
            .pCodeRange(Arrays.asList("10001", "10005"))
            .build();
   }

   private BatterySearchQuery initInvalidSearchQuery() {
      return BatterySearchQuery.builder()
            .pageSize(5)
            .pageNumber(0)
            .build();
   }

   private Mono<SearchResultEntity> mockResultSet() {
      return Mono.just(
            SearchResultEntity.builder()
                  .total(5L)
                  .data(mockData())
                  .avgUsage(BigDecimal.valueOf(7.5))
                  .build()
      );
   }

   private List<BatteryEntity> mockData() {
      return Arrays.asList(
            BatteryEntity.builder().name("bat-1").postcode("10001").wattCapacity(BigDecimal.valueOf(1)).createdDate(Instant.now()).build(),
            BatteryEntity.builder().name("bat-2").postcode("10002").wattCapacity(BigDecimal.valueOf(2)).createdDate(Instant.now()).build(),
            BatteryEntity.builder().name("bat-3").postcode("10003").wattCapacity(BigDecimal.valueOf(3)).createdDate(Instant.now()).build(),
            BatteryEntity.builder().name("bat-4").postcode("10004").wattCapacity(BigDecimal.valueOf(4)).createdDate(Instant.now()).build(),
            BatteryEntity.builder().name("bat-5").postcode("10005").wattCapacity(BigDecimal.valueOf(5)).createdDate(Instant.now()).build()
      );
   }
}
