package com.power.plant.battery;

import com.power.plant.common.throwable.BaseResponseEnum;
import com.power.plant.common.throwable.BizException;
import com.power.plant.common.throwable.ErrorWrapper;
import com.power.plant.domain.battery.pojo.BatteryDTO;
import com.power.plant.domain.battery.pojo.BatteryPageResponse;
import com.power.plant.domain.battery.pojo.BatterySearchQuery;
import com.power.plant.domain.battery.service.BatteryService;
import org.hamcrest.collection.IsCollectionWithSize;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@WebFluxTest
public class BatteryResourceTest {

   @MockBean
   BatteryService batteryService;

   @Autowired
   WebTestClient webTestClient;

   @Test
   @DisplayName("POST >> /api/v1/batteries >> Created")
   public void bulkCreateBatteries_Success() {
      List<BatteryDTO> mock = intiMockBatteries();
      when(batteryService.insertBatch(mock)).thenReturn(Flux.fromIterable(mock));

      webTestClient.post()
            .uri("/api/v1/batteries")
            .body(Flux.fromIterable(mock), BatteryDTO.class)
            .exchange()
            .expectStatus().isCreated();
   }

   @Test
   @DisplayName("POST >> /api/v1/batteries >> EmptyList >> Created")
   public void bulkCreateBatteries_EmptyList_Created() {
      List<BatteryDTO> mock = Collections.emptyList();
      when(batteryService.insertBatch(mock)).thenReturn(Flux.empty());

      webTestClient.post()
            .uri("/api/v1/batteries")
            .body(Flux.empty(), BatteryDTO.class)
            .exchange()
            .expectStatus().isCreated();
   }

   @Test
   @DisplayName("GET >> /api/v1/batteries/query >> Success")
   public void queryBatteries_Success() {

      BatterySearchQuery mockQuery = initSearchQuery();
      when(batteryService.query(any())).thenReturn(mockResultSet());

      webTestClient.post()
            .uri("/api/v1/batteries/query")
            .body(Mono.just(mockQuery), BatterySearchQuery.class)
            .exchange()
            .expectStatus().isOk()
            .expectBody(BatteryPageResponse.class)
            .value(BatteryPageResponse::getAvgUsage, IsEqual.equalTo(BigDecimal.valueOf(7.5)))
            .value(BatteryPageResponse::getTotal, IsEqual.equalTo(5L))
            .value(BatteryPageResponse::getData, IsCollectionWithSize.hasSize(5));
   }

   @Test
   @DisplayName("GET >> /api/v1/batteries/query >> InvalidSearchQuery >> ReturnError")
   public void queryBatteries_InvalidSearchQuery_ReturnError() {

      BatterySearchQuery mockQuery = initInvalidSearchQuery();
      when(batteryService.query(any())).thenThrow(BizException.builder()
            .errorEnum(BaseResponseEnum.PARAMS_ARE_MISSING)
            .build());

      webTestClient.post()
            .uri("/api/v1/batteries/query")
            .body(Mono.just(mockQuery), BatterySearchQuery.class)
            .exchange()
            .expectStatus().isOk()
            .expectBody(ErrorWrapper.class)
            .value(ErrorWrapper::getCode, IsEqual.equalTo(BaseResponseEnum.PARAMS_ARE_MISSING.getCode()))
            .value(ErrorWrapper::getMsg, IsEqual.equalTo(BaseResponseEnum.PARAMS_ARE_MISSING.getMsg()));
   }

   private List<BatteryDTO> intiMockBatteries() {
      return Arrays.asList(
            BatteryDTO.builder().name("bat-1").postcode("10001").wattCapacity(BigDecimal.valueOf(2000)).build(),
            BatteryDTO.builder().name("bat-2").postcode("10002").wattCapacity(BigDecimal.valueOf(3000)).build(),
            BatteryDTO.builder().name("bat-3").postcode("10003").wattCapacity(BigDecimal.valueOf(1000)).build(),
            BatteryDTO.builder().name("bat-4").postcode("10004").wattCapacity(BigDecimal.valueOf(2500)).build(),
            BatteryDTO.builder().name("bat-5").postcode("10005").wattCapacity(BigDecimal.valueOf(1200)).build(),
            BatteryDTO.builder().name("bat-6").postcode("10006").wattCapacity(BigDecimal.valueOf(4200)).build()
      );
   }

   private BatterySearchQuery initSearchQuery() {
      return BatterySearchQuery.builder()
            .pageSize(5)
            .pageNumber(0)
            .sortBy("name")
            .pCodeRange(Arrays.asList("10001", "10005"))
            .build();
   }

   private BatterySearchQuery initInvalidSearchQuery() {
      return BatterySearchQuery.builder()
            .pageSize(5)
            .pageNumber(0)
            .build();
   }

   private Mono<BatteryPageResponse> mockResultSet() {
      return Mono.just(
            BatteryPageResponse.builder()
                  .total(5L)
                  .data(mockData())
                  .avgUsage(BigDecimal.valueOf(7.5))
                  .build()
      );
   }

   private List<BatteryDTO> mockData() {
      return Arrays.asList(
            BatteryDTO.builder().name("bat-1").postcode("10001").wattCapacity(BigDecimal.valueOf(1)).build(),
            BatteryDTO.builder().name("bat-2").postcode("10002").wattCapacity(BigDecimal.valueOf(2)).build(),
            BatteryDTO.builder().name("bat-3").postcode("10003").wattCapacity(BigDecimal.valueOf(3)).build(),
            BatteryDTO.builder().name("bat-4").postcode("10004").wattCapacity(BigDecimal.valueOf(4)).build(),
            BatteryDTO.builder().name("bat-5").postcode("10005").wattCapacity(BigDecimal.valueOf(5)).build()
      );
   }
}
