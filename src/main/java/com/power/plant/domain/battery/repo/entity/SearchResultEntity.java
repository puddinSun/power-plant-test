package com.power.plant.domain.battery.repo.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Builder(toBuilder = true)
@ToString
public class SearchResultEntity {

   private Long total;
   private List<BatteryEntity> data;
   private BigDecimal avgUsage;

}
