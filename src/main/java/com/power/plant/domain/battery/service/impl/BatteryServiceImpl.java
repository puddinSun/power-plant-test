package com.power.plant.domain.battery.service.impl;

import com.power.plant.common.throwable.BaseResponseEnum;
import com.power.plant.common.throwable.BizException;
import com.power.plant.domain.battery.mapper.BatteryMapper;
import com.power.plant.domain.battery.pojo.BatteryDTO;
import com.power.plant.domain.battery.pojo.BatteryPageResponse;
import com.power.plant.domain.battery.pojo.BatterySearchQuery;
import com.power.plant.domain.battery.repo.BatteryRepository;
import com.power.plant.domain.battery.repo.entity.BatteryEntity;
import com.power.plant.domain.battery.repo.entity.BatteryPageQueryEntity;
import com.power.plant.domain.battery.repo.entity.SearchResultEntity;
import com.power.plant.domain.battery.service.BatteryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class BatteryServiceImpl implements BatteryService {

   @Autowired
   private BatteryRepository repository;

   private final BatteryMapper mapper = new BatteryMapper();

   @Override
   public Flux<BatteryDTO> insertBatch(List<BatteryDTO> batteries) {

      List<BatteryEntity> entities = batteries.stream()
            .map(mapper::toEntity)
            .collect(Collectors.toList());

      return repository.saveAll(entities).map(mapper::toDto);
   }

   @Override
   public Mono<BatteryPageResponse> query(BatterySearchQuery searchQuery) {

      if (CollectionUtils.isEmpty(searchQuery.getPCodeRange())) {
         throw new BizException(BaseResponseEnum.PARAMS_ARE_MISSING);
      }

      BatteryPageQueryEntity pageEntity = BatteryPageQueryEntity.builder()
            .pageNumber(searchQuery.getPageNumber())
            .pageSize(searchQuery.getPageSize())
            .pCodeRange(searchQuery.getPCodeRange())
            .build();

      Mono<SearchResultEntity> search = repository.search(pageEntity);
      return search.map(mapper::toSearchEntity);
   }
}
