package com.power.plant.domain.battery.mapper;

import com.power.plant.common.mapper.ObjectMapper;
import com.power.plant.common.utils.ObjectUtils;
import com.power.plant.domain.battery.pojo.BatteryDTO;
import com.power.plant.domain.battery.pojo.BatteryPageResponse;
import com.power.plant.domain.battery.repo.entity.BatteryEntity;
import com.power.plant.domain.battery.repo.entity.SearchResultEntity;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class BatteryMapper implements ObjectMapper<BatteryEntity, BatteryDTO> {

   @Override
   public BatteryEntity toEntity(BatteryDTO planDTO) {

      return BatteryEntity.builder()
            .id(ObjectUtils.toObjectId(planDTO.getId()))
            .name(planDTO.getName())
            .postcode(planDTO.getPostcode())
            .wattCapacity(planDTO.getWattCapacity())
            .build();
   }

   @Override
   public BatteryDTO toDto(BatteryEntity planEntity) {

      return BatteryDTO.builder()
            .id(ObjectUtils.toString(planEntity.getId()))
            .name(planEntity.getName())
            .postcode(planEntity.getPostcode())
            .wattCapacity(planEntity.getWattCapacity())
            .build();
   }

   public BatteryPageResponse toSearchEntity(SearchResultEntity entity) {
      return BatteryPageResponse.builder()
            .total(entity.getTotal())
            .avgUsage(entity.getAvgUsage())
            .data(entity.getData()
                  .stream()
                  .map(this::toDto)
                  .collect(Collectors.toList()))
            .build();
   }
}
