package com.power.plant.domain.battery.service;

import com.power.plant.domain.battery.pojo.BatteryDTO;
import com.power.plant.domain.battery.pojo.BatteryPageResponse;
import com.power.plant.domain.battery.pojo.BatterySearchQuery;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

public interface BatteryService {

   Flux<BatteryDTO> insertBatch(List<BatteryDTO> batteries);

   Mono<BatteryPageResponse> query(BatterySearchQuery params);
}
