package com.power.plant.domain.battery.repo.entity;

import com.power.plant.common.entity.WithCreatedDate;
import com.power.plant.common.entity.WithId;
import com.power.plant.common.entity.WithUpdatedDate;
import lombok.Builder;
import lombok.Getter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;

@Builder(toBuilder = true)
@Getter
@Document("batteries")
public class BatteryEntity implements WithId<ObjectId>, WithCreatedDate<Instant>, WithUpdatedDate<Instant>, Serializable {

    @Id
    ObjectId id;

    @Field("name")
    String name;

    @Field("postcode")
    String postcode;

    @Field("watt_capacity")
    BigDecimal wattCapacity;

    @Field("created_date")
    Instant createdDate;

    @Field("updated_date")
    Instant updatedDate;
}
