package com.power.plant.domain.battery.pojo;

import com.power.plant.common.pojo.PageResultDTO;
import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;
import java.util.List;

@Builder(toBuilder = true)
@Getter
public class BatteryPageResponse implements PageResultDTO<BatteryDTO> {

   private Long total;
   private List<BatteryDTO> data;
   private BigDecimal avgUsage;
}
