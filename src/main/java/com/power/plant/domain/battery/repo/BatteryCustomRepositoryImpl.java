package com.power.plant.domain.battery.repo;

import com.power.plant.domain.battery.repo.entity.BatteryPageQueryEntity;
import com.power.plant.domain.battery.repo.entity.SearchResultEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.data.mongodb.core.aggregation.LimitOperation;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.aggregation.ProjectionOperation;
import org.springframework.data.mongodb.core.aggregation.SkipOperation;
import org.springframework.data.mongodb.core.aggregation.SortOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@Repository
@Slf4j
public class BatteryCustomRepositoryImpl implements BatteryCustomRepository {

   @Autowired
   ReactiveMongoTemplate template;

   @Override
   public Mono<SearchResultEntity> search(BatteryPageQueryEntity pageQuery) {
      List<String> pCodeRange = pageQuery.getPCodeRange();
      String min = pCodeRange.get(0);
      String max = pCodeRange.get(1);

      MatchOperation match = new MatchOperation(Criteria.where("postcode").gte(min).lte(max));
      SortOperation sortByName = Aggregation.sort(Sort.Direction.ASC, "name");

      SkipOperation skip = Aggregation.skip((long) pageQuery.getPageNumber() * pageQuery.getPageSize());
      LimitOperation limit = Aggregation.limit(pageQuery.getPageSize());

      GroupOperation group = Aggregation.group()
            .push("$$ROOT").as("data")
            .count().as("total")
            .avg("$watt_capacity").as("avgUsage");

      ProjectionOperation project = Aggregation.project("total", "data", "avgUsage");
      Flux<SearchResultEntity> batteries = template.aggregate(
            Aggregation.newAggregation(match, sortByName, skip, limit, group, project),
            "batteries",
            SearchResultEntity.class
      );

      return batteries.next();
   }
}
