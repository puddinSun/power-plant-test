package com.power.plant.domain.battery.pojo;

import com.power.plant.common.pojo.BaseDTO;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

@Builder(toBuilder = true)
@Getter
@Jacksonized
public class BatteryDTO extends BaseDTO {

    String id;

    @NotBlank(message = "name is mandatory")
    String name;

    @NotBlank(message = "postcode is mandatory")
    String postcode;

    BigDecimal wattCapacity;
}
