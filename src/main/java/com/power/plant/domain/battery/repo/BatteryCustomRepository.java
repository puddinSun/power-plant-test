package com.power.plant.domain.battery.repo;

import com.power.plant.domain.battery.repo.entity.BatteryPageQueryEntity;
import com.power.plant.domain.battery.repo.entity.SearchResultEntity;
import reactor.core.publisher.Mono;

public interface BatteryCustomRepository {

   Mono<SearchResultEntity> search(BatteryPageQueryEntity pageQuery);
}
