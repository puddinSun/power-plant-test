package com.power.plant.domain.battery.pojo;

import com.power.plant.common.pojo.BaseDTO;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

import java.util.List;

@Builder(toBuilder = true)
@Getter
@Jacksonized
public class BatterySearchQuery extends BaseDTO {

   private int pageSize;

   private int pageNumber;

   private String sortBy;

   private List<String> pCodeRange;
}
