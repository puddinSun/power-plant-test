package com.power.plant.domain.battery.repo;

import com.power.plant.domain.battery.repo.entity.BatteryEntity;
import org.bson.types.ObjectId;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BatteryRepository extends
      ReactiveCrudRepository<BatteryEntity, ObjectId>, BatteryCustomRepository {
}
