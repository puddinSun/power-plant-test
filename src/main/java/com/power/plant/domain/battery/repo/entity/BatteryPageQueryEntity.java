package com.power.plant.domain.battery.repo.entity;

import com.power.plant.common.entity.PageEntity;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.util.List;

@Builder(toBuilder = true)
@Getter
public class BatteryPageQueryEntity implements PageEntity<BatteryEntity> {

   private int pageNumber;
   private int pageSize;
   private String sortBy;
   private List<String> pCodeRange;
}
