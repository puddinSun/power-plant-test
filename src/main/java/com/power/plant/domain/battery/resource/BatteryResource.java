package com.power.plant.domain.battery.resource;

import com.power.plant.domain.battery.pojo.BatteryDTO;
import com.power.plant.domain.battery.pojo.BatteryPageResponse;
import com.power.plant.domain.battery.pojo.BatterySearchQuery;
import com.power.plant.domain.battery.service.BatteryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@RestController
@RequestMapping("/api/v1/batteries")
@RequiredArgsConstructor
public class BatteryResource {

   private final BatteryService batteryService;

   @PostMapping
   public ResponseEntity<Flux<BatteryDTO>> insertBatch(@RequestBody List<BatteryDTO> batteries) {
      return ResponseEntity
            .status(HttpStatus.CREATED)
            .body(batteryService.insertBatch(batteries));
   }

   @PostMapping("/query")
   public ResponseEntity<Mono<BatteryPageResponse>> query(@RequestBody BatterySearchQuery searchQuery) {
      return ResponseEntity.ok(batteryService.query(searchQuery));
   }

}
