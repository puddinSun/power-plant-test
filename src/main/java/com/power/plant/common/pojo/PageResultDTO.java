package com.power.plant.common.pojo;

import java.util.List;

public interface PageResultDTO<T> {

   Long getTotal();

   List<T> getData();
}
