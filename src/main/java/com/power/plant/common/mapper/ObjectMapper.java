package com.power.plant.common.mapper;

public interface ObjectMapper<T, S> {

    T toEntity(S s);

    S toDto(T t);
}
