package com.power.plant.common.entity;

public interface WithUpdatedDate<T> {

    T getUpdatedDate();
}
