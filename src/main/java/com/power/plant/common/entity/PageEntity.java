package com.power.plant.common.entity;

public interface PageEntity<T> {

   int getPageSize();
   int getPageNumber();
   String getSortBy();
}
