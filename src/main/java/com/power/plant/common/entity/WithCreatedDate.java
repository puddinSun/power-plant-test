package com.power.plant.common.entity;

public interface WithCreatedDate<T> {

    T getCreatedDate();
}
