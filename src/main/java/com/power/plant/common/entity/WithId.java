package com.power.plant.common.entity;

public interface WithId<T> {

    T getId();
}
