package com.power.plant.common.throwable;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum BaseResponseEnum {

   PARAMS_ARE_MISSING("001", "error.params.missing");

   private final String code;
   private final String msg;
}
