package com.power.plant.common.throwable;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import reactor.core.publisher.Mono;

@ControllerAdvice
public class GlobalExceptionHandler {

   @ExceptionHandler(BizException.class)
   public ResponseEntity<Mono<ErrorWrapper>> handleException(BizException exception) {
      return ResponseEntity.status(HttpStatus.OK)
            .body(Mono.just(ErrorWrapper.builder()
                  .code(exception.getErrorEnum().getCode())
                  .msg(exception.getErrorEnum().getMsg())
                  .build()));
   }
}
