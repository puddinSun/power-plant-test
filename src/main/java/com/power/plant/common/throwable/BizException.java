package com.power.plant.common.throwable;

import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
@Builder(toBuilder = true)
public class BizException extends RuntimeException {

   private final BaseResponseEnum errorEnum;

}
