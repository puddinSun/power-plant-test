package com.power.plant.common.throwable;

import lombok.Builder;
import lombok.Getter;

@Builder(toBuilder = true)
@Getter
public class ErrorWrapper {

   String code;
   String msg;
}
