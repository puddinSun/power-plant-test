package com.power.plant.common.utils;

import lombok.experimental.UtilityClass;
import org.bson.types.ObjectId;

@UtilityClass
public class ObjectUtils {

   public String toString(ObjectId objectId) {
      return objectId == null ? null : objectId.toString();
   }

   public ObjectId toObjectId(String id) {
      return id == null ? null : new ObjectId(id);
   }
}
