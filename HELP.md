# Project information

* Java 11 
* Gradle
* Docker Compose
* Mongodb 6.0.4
* Code coverage: ~90%

# Test

* Junit5
* Mockito
* Mongodb Embedded

# Run project
``
./gradlew clean build
``

### Server information

* Elastic IP address: 
``
http://ec2-18.143.20.53.ap-southeast-1.compute.amazonaws.com/
``
* IP address: 
``
18.143.20.53
``
* Port: 8080


### API specs

Please follow APIs specs for performing testing:

#### Init items (list of items)
* Request
> Method: POST

> URL: http://localhost:8080/api/v1/baterries

> Sample request:
``
[
    {
        "name": "bat-1",
        "postcode": "10001",
        "wattCapacity": 20000
    },
    {
        "name": "bat-2",
        "postcode": "10002",
        "wattCapacity": 20000
    }
]
``

* Response

Response will be the same as request payload.

#### Search items by postcode range
* Request
> Method: POST

> URL: http://localhost:8080/api/v1/baterries/query

> Sample request:

``
{
    "pageSize": 5,
    "pageNumber": 0,
    "pCodeRange": [
        "10001",
        "10002"
    ],
    "sortBy": "id+DESC|name+ASC"
}
``
* Response

``
[
    {
        "name": "bat-1",
        "postcode": "10001",
        "wattCapacity": 20000
    },
    {
        "name": "bat-2",
        "postcode": "10002",
        "wattCapacity": 20000
    }
]
``