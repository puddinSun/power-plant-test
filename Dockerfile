FROM openjdk:8-jdk-alpine

ENV ACTIVE_PROFILE=staging
ARG JAR_FILE=build/libs/power-plant-0.0.1-SNAPSHOT.jar

MAINTAINER tmnhat <trinhminhnhat2011@gmail.com>
COPY ${JAR_FILE} plant.jar
ENTRYPOINT ["java","-jar", "-Dspring.profiles.active=${ACTIVE_PROFILE}", "plant.jar"]
EXPOSE 8081
